import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

public class ArrayInitiationCallable implements Runnable {

    private Random random;
    private int[][] array;
    private int[] arraySumRow;
    private int rowIndex;
    private CountDownLatch countDownLatch;

    public ArrayInitiationCallable(int[][] array, int[] arraySumRow, int rowIndex, CountDownLatch countDownLatch){
        this.array = array;
        this.arraySumRow = arraySumRow;
        this.rowIndex = rowIndex;
        this.countDownLatch = countDownLatch;
        this.random = new Random();
    }

    @Override
    public void run(){
        for (int j = 0; j < array[rowIndex].length; j++){
            array[rowIndex][j] = random.nextInt(10);
        }
        arraySumRow[rowIndex] = SummOfRow(array, rowIndex);
        countDownLatch.countDown();
    }

    private int SummOfRow(int[][] array, int i){
        int sum = 0;
        for(int j = 0; j < array[i].length; j++){
            sum += array[i][j];
        }
        return sum;
    }
}
