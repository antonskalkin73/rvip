import java.util.Random;
import java.util.concurrent.*;

public class Main {
    public static void main(String[] args){
        new Main().start();
    }

    public void start(){
        long startTime = System.currentTimeMillis();

        System.out.println("Привет! Эта прога работает автоматически. Ничего не трогай!");
        Random random = new Random();


        int[][] array = new int[5][5];
        int[] arraySumRow = new int[array.length];
        int countThreads = 10;

        ExecutorService executorService = Executors.newFixedThreadPool(countThreads);
        CountDownLatch countDownLatch= new CountDownLatch(array.length);

        for (int i = 0; i < array.length; i++){
            executorService.submit(new ArrayInitiationCallable(array, arraySumRow, i, countDownLatch));
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executorService.shutdown();

        //System.out.println(arrayToString(array) + "\nТеперь обраотка массива по заданию...\n");

        executorService = Executors.newFixedThreadPool(countThreads);
        countDownLatch= new CountDownLatch(1);
        Future f = executorService.submit(new ArraySortRunnable(array, arraySumRow, 0, arraySumRow.length-1, countDownLatch, executorService));
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            arraySumRow =(int[]) f.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        executorService.shutdown();


        /*
        for(int i = 0; i < array.length-1; i++){
            int indexRowIsMinSum = getIndexNeedRow(arraySumRow, i);
            if(indexRowIsMinSum != i){
                replaceRows(array, arraySumRow, i, indexRowIsMinSum);
            }
        }
        */

        //System.out.println(arrayToString(array));

        System.out.println("Время работы всей программы:\t" + (System.currentTimeMillis()-startTime));
    }

    private void replaceRows(int[][] array, int[] arraySumRow, int indexUp, int indexDown){
        int[] tmpRow = array[indexUp];
        array[indexUp] = array[indexDown];
        array[indexDown] = tmpRow;

        int tmp = arraySumRow[indexUp];
        arraySumRow[indexUp] = arraySumRow[indexDown];
        arraySumRow[indexDown] = tmp;
    }

    private int getIndexNeedRow(int[] arraySumRow, int i){
        int indexRowIsMinSum = i++;
        for(; i < arraySumRow.length; i++){
            indexRowIsMinSum = arraySumRow[i] < arraySumRow[indexRowIsMinSum] ? i : indexRowIsMinSum;
        }
        return indexRowIsMinSum;
    }

    private String arrayToString(int[][] array){
        StringBuilder stringBuilder = new StringBuilder();
        for (int[] anArray : array) {
            for (int anAnArray : anArray) {
                stringBuilder.append(anAnArray).append("\t");
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}


