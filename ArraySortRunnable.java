import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.*;

public class ArraySortRunnable implements Callable<int[]> {

    private int[][] array;
    private int[] arraySumRow;
    private int lowIndex;
    private int highIndex;
    private CountDownLatch countDownLatch;
    private ExecutorService executorService;

    public ArraySortRunnable(int[][] array, int[] arraySumRow, int lowIndex, int highIndex, CountDownLatch countDownLatch, ExecutorService executorService){
        this.array = array;
        this.arraySumRow = arraySumRow;
        this.lowIndex = lowIndex;
        this.highIndex = highIndex;
        this.countDownLatch = countDownLatch;
        this.executorService = executorService;
    }

    @Override
    public int[] call(){ //сортировка слиянием
        arraySumRow = sortMerge(arraySumRow);

        countDownLatch.countDown();

        return arraySumRow;
    }

    private int[] sortMerge(int[] arr) {
        int len = arr.length;
        if (len < 2) return arr;
        int middle = len / 2;

        CountDownLatch tmpCDL= new CountDownLatch(2);


        int[] tmp1 = new int[0];; //= sortMerge(Arrays.copyOfRange(arr, 0, middle));
        Future f1 = executorService.submit(new ArraySortRunnable(array, Arrays.copyOfRange(arr, 0, middle), 0, middle, tmpCDL, executorService));
        int[] tmp2 = new int[0]; //= sortMerge(Arrays.copyOfRange(arr, middle, len));
        Future f2 = executorService.submit(new ArraySortRunnable(array, Arrays.copyOfRange(arr, middle, len), middle, len, tmpCDL, executorService));
        try {
            tmpCDL.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            tmp1 =(int[]) f1.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try {
            tmp2 =(int[]) f2.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        executorService.shutdown();

        return merge(tmp1,tmp2);
    }

    private int[] merge(int[] arr_1, int[] arr_2) {
        int len_1 = arr_1.length, len_2 = arr_2.length;
        int a = 0, b = 0, len = len_1 + len_2; // a, b - счетчики в массивах
        int[] result = new int[len];
        for (int i = 0; i < len; i++) {
            if (b < len_2 && a < len_1) {
                if (arr_1[a] > arr_2[b])
                    result[i] = arr_2[b++];
                else
                    result[i] = arr_1[a++];
            } else if (b < len_2) {
                result[i] = arr_2[b++];
            } else {
                result[i] = arr_1[a++];
            }
        }
        return result;
    }

}